package com.corenetworks.hibernate.foro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForoCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForoCoreApplication.class, args);
	}
}
