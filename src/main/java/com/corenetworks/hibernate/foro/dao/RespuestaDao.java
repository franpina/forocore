package com.corenetworks.hibernate.foro.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.foro.model.Respuesta;


@Repository
@Transactional
public class RespuestaDao {

	
	  @PersistenceContext
		private EntityManager entityManager;
	    /*
	     * Almacena la respuesta en la base de datos
	     */
	    public void create(Respuesta respuesta) {
	    	entityManager.persist(respuesta);
	    	return;
	    }
	    
	    @SuppressWarnings("unchecked")
		public List<Respuesta> getAll(){
	    	return entityManager
	    			.createQuery("select r from Respuesta r")
	    			.getResultList();
	    }
	    
	    public Respuesta getById(long id) {
	    		return entityManager.find(Respuesta.class, id);
	    		
	    	
	    }
	    
	 	
}
