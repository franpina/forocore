package com.corenetworks.hibernate.foro.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import com.corenetworks.hibernate.foro.beans.RespuestaBean;
import com.corenetworks.hibernate.foro.beans.PreguntaBean;
import com.corenetworks.hibernate.foro.dao.RespuestaDao;
import com.corenetworks.hibernate.foro.dao.PreguntaDao;
import com.corenetworks.hibernate.foro.model.Respuesta;
import com.corenetworks.hibernate.foro.model.Pregunta;
import com.corenetworks.hibernate.foro.model.Profesor;

@Controller
public class PreguntaController {
	@Autowired
	private PreguntaDao preguntaDao;
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private RespuestaDao respuestaDao;

	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("pregunta", new PreguntaBean());
		return "submit";
	}

	@PostMapping(value = "/submit/newpregunta")
	public String submit(@ModelAttribute("preguntaRegister") PreguntaBean preguntaBean, Model model) {
		// userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(),
		// r.getPassword()));
		// crear un Post
		// Obtener el autor desde la sesión
		// Asignar el autor al post
		// Hacer persistente el post
		Pregunta pregunta = new Pregunta();
		pregunta.setTitulo(preguntaBean.getTitulo());
		pregunta.setContenido(preguntaBean.getContenido());
		

		Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
		pregunta.setProfesor(profesor);
		preguntaDao.create(pregunta);
		//profesor.getPosts().add(profesor);

		return "redirect:/";
	}

	@GetMapping(value = "/pregunta/{id}")
	public String detail(@PathVariable("id") long id, Model modelo) {
		// modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.

		Pregunta result = null;
		if ((result = preguntaDao.getById(id)) != null) {
			modelo.addAttribute("pregunta", result);
			 modelo.addAttribute("respuestaForm", new RespuestaBean());
			return "preguntadetail";
		} else
			return "redirect:/";
	}
	
	@PostMapping(value = "/submit/newrespuesta")
	public String submitRespuesta(@ModelAttribute("respuestaForm") 	RespuestaBean respuestaBean, Model model) {
		//autor que esta logueado
		Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
		
		Respuesta respuesta = new Respuesta();
		respuesta.setProfesor(profesor);
		
		Pregunta pregunta= preguntaDao.getById(respuestaBean.getPregunta_id());
		respuesta.setPregunta(pregunta);
		respuesta.setContenido(respuestaBean.getContenido());
		respuestaDao.create(respuesta);
		//pregunta.getRespuestas().add(respuesta);
		//profesor.getRespuestas().add(respuestas);
	
		
		

		return "redirect:/pregunta/" + respuestaBean.getPregunta_id();
	}
}