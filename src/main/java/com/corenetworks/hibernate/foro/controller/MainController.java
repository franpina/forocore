package com.corenetworks.hibernate.foro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.foro.dao.PreguntaDao;

@Controller
public class MainController {

	@Autowired
	PreguntaDao preguntaDao;
	
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		modelo.addAttribute("preguntas", preguntaDao.getAll());
		
		return "index";
		
	}
}