package com.corenetworks.hibernate.foro.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.foro.beans.LoginBean;
import com.corenetworks.hibernate.foro.dao.ProfesorDao;
import com.corenetworks.hibernate.foro.model.Profesor;

@Controller
public class LoginController {
	@Autowired
	private ProfesorDao userDao;
	
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/signin")
	public String showForm(Model model) {
		model.addAttribute("userLogin", new LoginBean());
		return "login";
	}

	@PostMapping(value = "/login")
	public String submit(@ModelAttribute("userLogin") LoginBean loginBean, Model model) {
		// userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(),
		// r.getPassword()));
		Profesor u = userDao.getByEmailAndPassword(loginBean.getEmail(), loginBean.getPassword());
		if (u != null) {
			httpSession.setAttribute("userLoggedIn", u);
		    return "redirect:/";
		} else {
			model.addAttribute("error","Error de validación");
			return "login";
		}
	}
	
	
	@GetMapping(value = "/logout")
	public String logout(Model model) {
		httpSession.removeAttribute("userLoggedIn");
		return "redirect:/";
	}
}