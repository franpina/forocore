package com.corenetworks.hibernate.foro.beans;


import java.util.Date;

import com.corenetworks.hibernate.foro.model.Profesor;

public class PreguntaBean {

	private String titulo;
	
	private String contenido;
	private Date fecha;
	private Profesor autor;
	
	
	
	public Profesor getAutor() {
		return autor;
	}


	public void setAutor(Profesor autor) {
		this.autor = autor;
	}


	public PreguntaBean() {
		super();
	}

	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
