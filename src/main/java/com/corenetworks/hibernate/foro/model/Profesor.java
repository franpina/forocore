package com.corenetworks.hibernate.foro.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Profesor {

	
	public Profesor() {
		
	
	}
	
	public Profesor(String nombre, String ciudad, String email, String password) {
		
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.email = email;
		this.password = password;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column
	private String ciudad;
	
	@Column
	private String email;
	
	@Column
	@ColumnTransformer(write="MD5(?)")
	private String password;
	
	/*@OneToMany(mappedBy="autor" ,fetch =FetchType.EAGER)	
	private Set<Pregunta> posts= new HashSet<>();
	
	@OneToMany(mappedBy="user" ,fetch =FetchType.EAGER)	
	private Set<Comment> comments = new HashSet<>();*/
	
	/*
	public Set<Pregunta> getPosts() {
		return posts;
	}

	public void setPosts(Set<Pregunta> posts) {
		this.posts = posts;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}*/

	@Column
	@CreationTimestamp
	private Date fechaAlta;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nombre=" + nombre + ", ciudad=" + ciudad + ", email=" + email + ", password="
				+ password + ", fechaAlta=" + fechaAlta + "]";
	}
	
	
	
	
}
