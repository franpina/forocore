package com.corenetworks.hibernate.foro.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.OneToMany;

@Entity
@Table(name="pregunta")
public class Pregunta {

		public Pregunta() {
			super();
	
		}
		
		public Pregunta(String titulo, String contenido) {
			
		
			this.titulo = titulo;
			this.contenido = contenido;
			
		}
		
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private long id;
		
		@Column
		@Lob
		private String titulo;
		
		
		
		@Column
		private String contenido;
		
		@Column
		@CreationTimestamp
		private Date fecha;
		
	
		@ManyToOne
		private Profesor profesor;
		
		@OneToMany(mappedBy="pregunta")
		private List<Respuesta> respuestas = new ArrayList<>();

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getTitulo() {
			return titulo;
		}

		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}

	

		public String getContenido() {
			return contenido;
		}

		public void setContenido(String contenido) {
			this.contenido = contenido;
		}

		public Date getFecha() {
			return fecha;
		}

		public void setFecha(Date fecha) {
			this.fecha = fecha;
		}

		
		public Profesor getProfesor() {
			return profesor;
		}

		public void setProfesor(Profesor profesor) {
			this.profesor = profesor;
		}
		
		public List<Respuesta> getRespuestas() {
					return respuestas;
				}
			
				public void setRespuestas(List<Respuesta> respuestas) {
					this.respuestas = respuestas;
				}

		
		
		
		
	}

